'use strict';

import * as THREE from 'three'
import {
    OBJLoader
} from 'three/examples/jsm/loaders/OBJLoader'
import {
    OrbitControls
} from 'three/examples/jsm/controls/OrbitControls'
import { VectorKeyframeTrack } from 'three';

var sceneBlock = document.getElementById('three');

var groupLinks;

fetch('http://steklosfera.vladweb001.beget.tech/rest/products/all')
    .then((resp) => resp.json())
    .then(data => {
        groupLinks = data
        addObjectToScene()
    });

var redirectID = '';

const sceneParams = {
    width: sceneBlock.offsetWidth,
    height: sceneBlock.offsetHeight
}

const scene = new THREE.Scene()
scene.background = new THREE.Color(0xc9c9c9);

const camera = new THREE.PerspectiveCamera(75, sceneParams.width / sceneParams.height, 0.1, 10000)
camera.position.set(0, 1000, 1000)

const renderer = new THREE.WebGLRenderer({
    antialias: true
})

const raycaster = new THREE.Raycaster();

renderer.setSize(sceneParams.width, sceneParams.height)

sceneBlock.appendChild(renderer.domElement)

initLight()

const addObjectToScene = () => {
    var loader = new OBJLoader()
    var box = new THREE.Box3()

    loader.load('../models/schema.obj', function (object) {
            box.setFromObject(object)

        scene.add(object)
    })
}

function initLight() {
    const lights = []

    lights[0] = new THREE.SpotLight(0xffffff, .7)
    lights[1] = new THREE.SpotLight(0xcccccc, .3)
    lights[2] = new THREE.SpotLight(0xcccccc, .6)
    lights[3] = new THREE.SpotLight(0xcccccc, .6)
    lights[4] = new THREE.SpotLight(0xcccccc, .35)
    lights[5] = new THREE.SpotLight(0xcccccc, .35)

    lights[0].position.set(0, 1199, 0)
    lights[1].position.set(0, -1984, -500)
    lights[2].position.set(-1000, -493, 58)
    lights[3].position.set(1000, -493, 58)
    lights[4].position.set(0, 100, -5000)
    lights[5].position.set(0, 100, 5000)

    scene.add(lights[0], lights[1], lights[2], lights[3], lights[4], lights[5])
}


const controlling = new OrbitControls(camera, renderer.domElement);


const mouse = new THREE.Vector2();

const coords = [];

var mouseMoving = false;
var clicked = false;

function onMouseMove(event) {
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

    coords.x = event.clientX
    coords.y = event.clientY

    if (clicked) {
        mouseMoving = true
    }
}

window.addEventListener('mousemove', onMouseMove, false);

window.addEventListener('click', onClick, false);

window.addEventListener('mouseup', () => {
    setTimeout(() => {
        mouseMoving = false
        clicked = false
    }, 100)
}, false);

window.addEventListener('mousedown', () => {
    clicked = true
}, false);

var INTERSECTED, materialName, tooltipElem;

function onClick(e) {
    if (tooltipElem) {
        tooltipElem.remove();
        tooltipElem = null
    }

    if (INTERSECTED) {
        let materialName = INTERSECTED.material.name;

        for (let i of groupLinks) {
            if (i.id === INTERSECTED.material.name) {
                materialName = i.title;
    
                toolTip(materialName)
    
                break;
            }
        }
    }
}

function animate() {
    requestAnimationFrame(animate)

    controlling.update()

    controlling.maxDistance = 1800
    controlling.minDistance = 1200

    renderer.render(scene, camera)

    raycaster.setFromCamera(mouse, camera);

    if (scene.children[6]) {

        var intersects = raycaster.intersectObjects(scene.children[6].children);

        if (intersects.length > 0) {
            if (INTERSECTED != intersects[0].object) {

                if (INTERSECTED) {
                    INTERSECTED.material.color.setHex(INTERSECTED.currentHex)
                }

                INTERSECTED = intersects[0].object;

                INTERSECTED.currentHex = INTERSECTED.material.color.getHex();

                for (let i of groupLinks) {
                    if (i.id === INTERSECTED.material.name) {
                        INTERSECTED.material.color.setHex(0xff0000);
                        redirectID = INTERSECTED.material.name;
                        break;
                    }
                }
            }
        } else {
            if (INTERSECTED) {
                INTERSECTED.material.color.setHex(INTERSECTED.currentHex);
            }

            INTERSECTED = null;
            materialName = null;
            redirectID = null;
        }
    }
}

function toolTip(name) {
    console.log(name)

    tooltipElem = document.createElement('div');
    tooltipElem.className = 'tooltip';


    var nameElem = document.createElement("span");
    nameElem.innerHTML = name
    tooltipElem.append(nameElem);

    for (let i of groupLinks) {
        if (i.id == redirectID) {
            i.group_links.forEach((data) => {
                var linkElem = document.createElement("a");
                linkElem.innerHTML = data.name
                linkElem.target = "_blank"

                linkElem.href = data.link

                tooltipElem.appendChild(linkElem)
            })
            break;
        }
    }

    document.body.append(tooltipElem);

    let left = coords.x - tooltipElem.offsetWidth / 2;

    let top = coords.y - tooltipElem.offsetHeight - 5;

    if (top < 0) {
        top = coords.y + sceneBlock.offsetHeight + 5;
    }

    tooltipElem.style.left = left + 'px';
    tooltipElem.style.top = top + 'px';
}

animate()

function onWindowResize() {

    camera.aspect = sceneBlock.offsetWidth / sceneBlock.offsetHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(sceneBlock.offsetWidth, sceneBlock.offsetHeight);

}

window.addEventListener('resize', onWindowResize, false);